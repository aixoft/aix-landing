import React from 'react';
import App from './App';

export default {
    title: 'Main Page',
    component: App,
};

export const WholePage = () => <App />;
