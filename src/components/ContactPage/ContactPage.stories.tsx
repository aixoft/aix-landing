import React from 'react';
import { ContactPage } from './ContactPage';

export default {
    title: 'Contact us Page',
    component: ContactPage,
};

export const ContactUsPage = () => <ContactPage />;
