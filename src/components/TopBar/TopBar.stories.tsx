import React from 'react';
import TopBar from './TopBar';

export default {
    title: 'Top Bar',
    component: TopBar,
};

export const DefaultTopBar = () => <TopBar />;
