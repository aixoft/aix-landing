import React from 'react';
import { AboutUsPage } from './AboutUsPage';

export default {
    title: 'About us Page',
    component: AboutUsPage,
};

export const Default = () => <AboutUsPage />;
