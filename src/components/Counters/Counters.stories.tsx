import React from 'react';
import { Counters } from './Counters';

export default {
    title: 'Counters',
    component: Counters,
};

export const SkillsCounters = () => <Counters />;
