import React from 'react';
import { TitlePage } from './TitlePage';

export default {
    title: 'Title Page',
    component: TitlePage,
};

export const Default = () => <TitlePage />;
