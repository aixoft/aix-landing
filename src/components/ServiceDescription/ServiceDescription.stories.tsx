import React from 'react';
import { ServiceDescription } from './ServiceDescription';

export default {
    title: 'Services description',
    component: ServiceDescription,
};

export const Description = () => <ServiceDescription />;
